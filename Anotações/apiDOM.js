(function(){
   /*API DOM
   - É a manipulação de elementos do arquivo html;
   - Cada elemento do arquivo html se torna um nó;

   - Percorrendo elementos:*/

   /*26 - 154*/
   var $main = document.querySelector('.main');
   //As propriedades abaixo, trazem todos os tipos de informações do html, como comentários, quebra de linhas, elementos html ...
   console.log( $main.parentNode ); //Retorna o elemento pai do 'main'
   console.log( $main.childNodes ); //Retorna todos os elementos filhos (array-like) do elemento 'main'
   console.log( $main.firstChild ); //Retorna o primeiro elemento filho do pai 'main'
   console.log( $main.lastChild );  //Retorna o último elemento filho do pai 'main'
   console.log( $main.nextSibling );//Retorna os elementos 'irmãos', que estão no mesmo 'nivel'

   /*26 - 155*/
   console.log( $main.nodeType );  //Retorna um Numero equivalente ao tipo - Element-1, Text-3, Comments-8, 9-Document e documentFragment-22
   console.log( $main.nodeValue ); //Retorna o valor do elemento
   console.log( $main.firsChild.nextSibling.nodeValue ); //Retorna o valor do elemento irmão
   console.log( $main.nodeName );  //Retorna o nome do elemento

   /** IMPORTANTE ***/ /*26 - 156*/
   //As propriedades abaixo não trazem elementos de texto, apenas os elementos HTML, diferente das propriedades citadas acima
   console.log( $main.children );      //Retorna todos os elementos HTML filhos (array-like) do elemento 'main' (diferente do childNodes) que são do tipo HtmlCollections
   console.log( $main.firstElementChild ); //Retorna o primeiro elemento filho, que é um elemento HTML
   console.log( $main.lastElementChild );  //Retorna o último elemento filho, que é um elemento HTML
   console.log( $main.nextElementSibling );//Retorna os elementos 'irmãos', que estão no mesmo 'nivel'
   console.log( $main.childElementCount ); //Retorna o total de elementos filhos

   //Métodos para Manipular o DOM
   console.log( $main.hasAttribute('data-js') );  //Verifica se o elemento possui determinado atributo/propriedade - True e False
   console.log( $main.hasAttributes() ); //Verifica se o elemento possui algum Atributo/propriedade informado - True e False

   /*26 - 157*/
   var $mainContent = document.querySelector('.main-content');
   var $mainHeader  = document.querySelector('.main-header');
   var $firstElement= $mainContent.firsChild;

   $mainContent.appendChild( $mainHeader ); //Move de posição o 'mainHeader', para a última posição dentro do 'mainContent'

   //1º Param: O Nó que será inserido
   //2º Param: Antes de qual elemento ele será inserido
   $mainContent.insertBefore( $mainHeader, $firstElement ); //Insere o $mainHeader dentro do $mainContent, antes do elemento $firstElement

   //Parametro booleano, para definir se deseja clonar o conteudo do elemento
   var $cloneMainHeader = $mainHeader.cloneNode(true); //Faz uma copia do objeto
   $mainContent.appendChild( $cloneMainHeader ); //Insere o elemento clonado

   console.log( $mainContent.hasChildNodes() ); //True ou False - Verifica se o elemento possui elementos filhos

   /*26 - 158*/
   var $mainContent = document.querySelector('.main-content');
   var $mainHeader  = document.querySelector('.main-header');
   var $firstElement= $mainContent.firsChild;

   $mainHeader.removeChild( $firstElement ); //Remove o elemento '$firstElement' do elemento '$mainHeader'
   $mainContent.replaceChild( $firstElement, $mainHeader ); //Substitui o elemento '$firstElement' pelo elemento '$mainHeader', o '$mainHeader' não é clonado, só troca de lugar

   var newTextNode = document.createTextNode('opa'); //Cria o elemento de 'texto'
   $mainHeader.appendChild( newTextNode ); //Insere o novo elemento dentro do $mainHeader

   var newElement = document.createElement('p'); //Cria um elemento Html, neste caso, um parágrafo
   newElement.appendChild( newTextNode ); //Insere o texto no elemento paragrafo
   $mainHeader.appendChild( newElement ); //Insere o elemento html dentro do $mainHeader

   /*26 - 159*/
   //MANIPULANDO ATRIBUTOS DO DOM
   //Todos os atributos do elemento DOM, podem ser lidos e alterados (get e set).
   //Ex.: id, class, name, value, data-js, ....

   var $mainContent = document.querySelector('.main-content');
   $mainContent.getAttribute('data-js'); //Retonar o valor do atributo passado por parametro
   $mainContent.setAttribute('data-js', 'main-data-js'); //Substitui a declaração do tributo de 'data-ja' para 'mains-data-js'

   //DOM - Manipular com Performance - 27 - 162
   //É criado um documento em Branco para que seja manipulado e quando estiver tudo certo, pode ser
   //injetdo no DOM, mantendo a performance da DOM, pois não é manipulado a DOM de forma direta
   document.createDocumentFragment();

   var fragment = document.createDocumentFragment(); //Documento em Branco
   var childP = document.createElement('p'); //Cria o elemento tipo paragrafo
   var textChildP = document.createTextNode('Texto da tag P!'); //Cria o elemento texto para o elemento

   childP.appendChild(textChildP);  //Adiciona o elemento texto a tag de paragrafo
   fragment.appendChild(child); //Insere o elemento dentro do arquivo em branco

   document.body.appendChild(fragment); //Atualiza o DOM, inserindo o arquivo fragmentado


   //DOM - Eventos /*27 - 163*/
   //DOMContentLoaded - Evento que será executado após carregar todo o arquivo HTML (DOM), garante que seja executado uma rotina que por exemplo, manipula o DOM, só após ter toda a página carregada, evitando assim, possiveis erros referente a chamada de um elemento que ainda não foi criado.
   function afterDomContentLoaded(){
      var childP = document.createElement('p'); //Cria o elemento tipo paragrafo
      document.body.appendChild(childP); //Adiciona o elemento ao DOM
   }
   //Será executado a function 'afterDomContentLoaded()', após o DOM ser totalmente carregado, pelo evento 'DOMContentLoaded'
   document.addEventListener('DOMContentLoaded', afterDomContentLoaded, false);


}());
