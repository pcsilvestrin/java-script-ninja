(function(win, doc){
   //win - Neste caso é a Window pricipal

   //Métodos
   win.alert();  //Gera um alerta no Navegador
   win.prompt('Quantos anos?'); //Gera uma pergunta no Navegador
   win.confirm('Deseja realmente excluir?'); //Mensagem de Confirmação - True ou False

   //****** 21 - 128 */
   console.log('inicio');
   win.setTimeOut(function(){  //Método Sincrono
	console.log('Passou TimeOut!');
   }, 1000); //O que está dentro da Função será executado APÓS UM SEGUNDO
   console.log('fim');

   //**** CONSOLE *****
   //* inicio
   //* fim
   //* Passou TimeOut!
   //****************** 	/

   console.log('inicio');
   win.setInterval(function(){
	console.log('Passou pela função do Interval!');
   }, 1000); //O que está dentro da Função será executado a CADA UM SEGUNDO
   console.log('fim');

   //**** CONSOLE ***** /
   //* inicio
   //* fim
   //* Passou pela função do Interval!
   //* Passou pela função do Interval!
   //* Passou pela função do Interval!
   //* .....
   //******************/

   //*** Utilizando o 'setTimeOut()' em modo Recursivo - 21 - 129*/
   var counter = 0;
   function timer(){
      console.log('timer', counter++);
      if (counter > 10)
         return; //Quebra a repetição
      setTimeOut(timer, 100); //A cada um segundo, enquanto o 'counter' for menor ou igual a 10 será executado o 'setTimeOut()'
   }

   //IMPORTANTE - Ao setar em uma variavel o 'setTimeOut()' ou 'setInteval()', a variavel terá o ID do metodo e
   //com esse ID, pode ser executado o 'clearTimeOut()' ou 'clearInterval()';

   //Utilizar o 'data-js' nas desclarações de elementos html, pois, dessa forma se torna mais prático
   //capturar o elemento, pois, a tag 'id' possui falhas e a tag 'class' é utilizada na estilização do componente (css)

   //HTML - Declarar os elementos com 'data-js'
   <button data-js="button"> Parar! </button>

   //JS - Capturando o Botão
   var $button = document.querySelector('[data-js="button"]');
   //*****************************************************************/

   console.log(doc.getElementById('id_1')); //Retorna o PRIMEIRO elemento do HTML que possui o id igual ao passado por parametro
   console.log(doc.getElementsByClassName('id_1')); //Retorna um ARRAY (HtmlCollection) com todos os elementos do HTML que possuem a mesma classe passada por parametro
   console.log(doc.getElementsByTagName('id_1')); //Retorna um ARRAY (HtmlCollection) com todos os elementos do HTML que possuem a mesma tag passada por parametro
   console.log(doc.getElementsByName('id_1')); //Retorna o elemento do HTML que possui o mesmo nome passado por parametro

   var $inputs;
   $inputs = doc.querySelector('input');    //Retorna apenas o primeiro elemento do tipo input que encontrar
   $inputs = doc.querySelectorAll('input'); //Retorna TODOS os elementos do tipo input que encontrar  - Array

   $inputs = doc.querySelector('.input'); //Retorna todos os ojetos com a class 'input'
   $inputs = doc.querySelector('#input'); //Retorna todos os ojetos com a id 'input'

   //FORMULÁRIOS
   var $inputUserName = doc.querySelector('#username');
   var $inputPassword = doc.querySelector('#password');
   var $button = doc.querySelector('#button'); //Captura o Botão que esta com a 'id' igual a 'button'

   //Apresenta os valores informados nos campos User Name e Password
   console.log($inputUserName.value, $inputPassword.value);

   //20 - 123
   $button.addEventListener();//Cria uma 'escuta' no elemento, para que quando ocorrer determinada situação, acionado um evento,
   //seja feito algo, no exemplo abaixo, quando clicar sobre o botão, será ativado o evento 'click' e nesse momento
   //será executado a função declarada como segundo parametro

   //1º Parametro: Evento que será 'escutado'
   //2º Parametro: A função que será executada ao assionar o evento, o parametro da função (event) é o objeto gerado pelo elemento ao ser executado o evento
   //3º Parametro: Por padrão False, define a ordem em que os eventos serão executados, Ex.
   //  div - a - input (Div possui um 'a href' e um input e todos possuem evento do click vinculado)
   //  - Quando o terceiro Parametro for FALSE e é clicado no Input, primeiro executa o evento input, depois do 'a href' e depois da DIV
   //  - Quando o terceiro Parametro for TRUE e é clicado no Input, é invertido o processo, primeiro executa o evento da Div, depois do 'a href' e depois do Input (isso é pouco usado)
   $button.addEventListener('click', function(event){
      //Não executa a função padrão do elemento, se for um submit, não será enviado as informações do formulário que é a ação padrão
      event.preventDefault();
      console.log('Click no Botão!');
   }, false);


   //Seção 25 - 153
   //Eventos adicionados através do 'addEventListener()', são cumulativos, ou seja,
   //pode ser injetado vários eventos para o click de um único elemento e
   //todos os eventos serão executados em sequência.
   $button.addEventListener('click', clickButton1, false);
   $button.addEventListener('click', clickButton2, false);

   //Utilizado para remover um determinado evento da Lista, antes de ser executado
   $button.removeEventListenerventListener('click', clickButton2, false);

   function clickButton1(event){
      console.log('Clicou no Botão 1!');
   }
   function clickButton2(event){
      console.log('Clicou no Botão 2!');
   }

}(window, document));
