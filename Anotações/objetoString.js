(function(){
   //Objeto String
   'Paulo'.length() //5

   'Paulo'.charAt(2); //u - Pega o valor na posição passada por parametro
     //ou
   'Paulo'[2]; //u

   var nome = 'Paulo';
   var nomeCompleto = nome.concat(' Cesar', ' Silvestrin'); //Concatena as Strings, retornando uma nova String sem alterar o valor da variavel principal

   nomeCompleto.indexOf('u');    //Retorna a posição em que a substring informada por parametro está na String, -1 não encontrou
   nomeCompleto.indexOf('u', 3); //O segundo parametro é para definir que a verificação deve ser realizada apartir deste indice
   nomeCompleto.lastIndexOf('u');//mesma funcionalidade do indexOf(), nesse caso a busca se inicia de Trás para Frente

   nomeCompleto.replace('Silvestrin', 'novo'); //Retorna uma nova string, substituindo a substring informada (só faz para o primeiro elemento encontrado)
   nomeCompleto.slice(0, 4); //Retorna uma nova String, com os primeiros 4 caracteres

   nome.split('u'); //Quebra a string quando encontrar essa substring, retornando um Array - Remove da String tudo o que for igual a substring
   //['Pa','lo]

   //Para substituir letras e substring em massa
   nome.split('u').join('t'); //Retorna a String 'Patlo', substituindo todos os 'u' da palavra

   nomeCompleto.substring(2, 4); //ulo - Funciona da mesma forma que utilizando o slice, porém, o segundo parametro é o indice final e não a quantidade de caracteres
   nomeCompleto.toLowerCase(); //paulo - Tudo em minusculo
   nomeCompleto.toUpperCase(); //PAULO - Tudo em maiusculo

}());
