(function(){
   //Arrays em JS são Objetos que possuem propriedades e metodos:
   var arr1 = ['Paulo', null, true, {bola: 'preta'}, function(){}];
   arr1.length; //Retorna a quantidade de elementos dentro do Array

   arr1.push('Novo Elemento'); //Metodo utilizado para adicionar um novo elemento no Array
   arr1.push(function soma(a,b){ return a + b }); //Pode ser adicionado qualquer tipo de elemento ao array

   //Setando valor direto na posição xxx, dessa forma, o JS cria o intervalo das posições entre 2 a 11 vazias
   arr1[12] = 'Doze';

   arr1.pop(); //Retorna o valor da ultima posição e Remove a última posição do Array
   //*********************/

   var arr2 = ['arroz', 'feijão', 'lasanha']; //Aula 12 - 74
   arr2.join(', '); //Concatena todas as propriedades valores em uma String, o parametro é o separador - R: 'arroz, feijão, lasanha'
   arr2.reverse();  //Inverte a posição dos valores do array - R: ['lasanha', 'feijão', 'arroz'];
   arr2.sort();     //Ordena os itens do Array em ordem alfabética
   //*********************/

   var arr3 = [1,2,3,4]; //Aula 13 - 77
   arr3.toString(); //Converte em string os elementos do Array, da mesma forma que o join(), a diferença é que utilizando o join(), é possivel passar por parametro o caracter que será utilizado como separador

   arr3.concat(5);  //Retorna um novo array, com os mesmos elementos do arr, porém adiciona o elemento passado por parametro no final do NOVO array
   //var brasil = sul.concat(sudeste); //Sul e Sudeste são Arrays que concatenados geram o Array 'brasil' //Challenge 13

   arr3.unshift(0); //Adiciona um novo elemento, porém, no INICIO do Array
   arr3.shift();    //Remove o primeiro elemento do Array
   //*********************/

   var arr4 = [1,2,3,4,5]; //Aula 13 - 78

   //Retorna uma parte do Array (um novo array), podendo passar dois parametros,
   //que são os indices do array, onde o primeiro parâmetro é o indice do primeiro elemento que será pego e
   //o segundo parametro é o indece final, será retornado todos os elementos que estão na posição do primeiro parametro a até o ultimo elemento antes do segundo indice informado
   arr4.slice(1); //Retorna um novo array, pegando todos os elementos do array, apartir do indice 1 elementos do array
   arr4.slice(1, 3); //Retorna um novo array, pegando todos os elementos do array, apartir do indice 1 até o ultime elemento antes do indice 3

   //Utilizado para Remover e Adicionar elementos no Array
   arr4.splice();  //Esse método Modifica o Array
   arr4.splice(1); //Será removido o elemento que está no indice informado
   arr4.splice(2,2); //Apartir do indice x (primeiro parametro), será removido 'n' elementos (segundo parametro)
   arr4.splice(1, 0, 'a'); //IMPORTANTE - Não remove nada, insere o elemento 'a' após o indice 1, pode ser passado mais elementos para serem adicionados
   arr4.splice(1, 4, 2, 3, 4); //IMPORTANTE - Após o indice 1, será removido 4 elementos do Array e será adicionado os elementos 2, 3 e 4
   //*********************/

   var arr5 = [1,2,3,4,5,6,7]; //Aula 13 - 79
   //Laço de Repetição forEach
   //Pode ser informado três parâmetros, porém o 2º e 3º parametros não são obrigatórios
   //1º Parametro: Item do Array
   //2º Parametro: Indice do Array
   //3º Parametro: O Próprio Array
   arr5.forEach(function(item, index, array){
      console.log(item, index, array);
   });

   //Utilizado para validar se todos os elementos são válidos perante a uma condição, por exemplo, se são maiores que 5, menores que 100, se um elemento for false, vai ser retornado false
   arr5.every(); //Retorna sempre True ou False
   var every = arr5.every(function(item){ //Percorre o array, o item é o elemento do Array
      return item < 5;
   });

   //Utilizado para validar se ao menos UM elemento do array é válido perante a uma condição, diferente do every()
   arr5.some(); //Retorna sempre True ou False
   var some = arr5.some(function(item){ //Percorre o array, o item é o elemento do Array
      return item < 5;
   });
   //*********************/

   var arr6 = [1,2,3,4,5,6,7]; //Aula 13 - 80

   //Percorre o Array assim como o forEach, porém, retorna um novo array apartir dos elementos retornados
   var map = arr6.map(function(item, index, array){
     return item + 1;
   });
   console.log(map); //map === [2,3,4,5,6,7,8]

   //Percorre o Array assim como o map, porém, é utilizado para filtrar os elementos que serão retornados - Retorna um novo array apartir dos elementos retornados
   var filter = arr6.filter(function(item, index, array){
     return item > 5;
   });
   console.log(filter); //filter === [6,7]

   //Pode ser usado em forma cadeada com o map, pois o map já retorna um array
   var map = arr6.map(function(item){
     return item + 1;
   }).filter(function(item){
     return item > 5;
   });
   console.log(map); //map === [6,7,8]
   //*********************/

   var arr7 = [1, 2, 3, 4, 5]; //Aula 14 - 84
   //Acumulado - O reduce percorre o array para retornar um unico valor
   //no exemplo abaixo, o "acumulado" recebe ele mesmo mais o valor do item
   //pode ser utlizado em calculos ou concatenação de Strings
   var reduce = arr7.reduce(function(acumulado, item, index, array){
     return acumulado + item;
   }, 0); //O zero é para iniciar o valor do "acumulado", se não informado o acumulado já sai recebendo o valor do primeiro item
   console.log(reduce); //15

   //Funciona da mesma forma do Reduce, porém, começa a percorrer o Array de Trás para Frete
   var reduceRight = arr7.reduceRight(function(acumulado, item, index, array){
     return acumulado + item;
   }, 0);
   console.log(reduceRight); //15
   //*********************/

   var arr8 = [1, 2, 3, 4, 5]; //Aula 14 - 85

   //Retorna o indice do Array que possui o valor passado por parametro
   //Se não existir, retorna -1
   console.log(arr8.indexOf( 3 )); //2

   //O segundo parametro, é o indice inicial, que apartir desse indice será verificado se o valor existe
   console.log(arr8.indexOf( 3, 2 )); //-1

   //Funciona da mesma forma do indexOf, porém a busca inicia apartir do ultimo elemento
   console.log(arr8.lastIndexOf( 3 )); //2

   //Verifica se o parametro passado é um Array - True e False
   Array.isArray(arr8);
   //*********************/

   //Array-like - 22 - 135 *****************
   //São objetos que possuem o formato de Array, porém, não é herdado do Array.prototype, desssa forma não possuem os metodos padrões para os arrays, como o ForEach, pop, slice, push, splice, ...
   //Exemplos de Array-like: arguments (possui os parametros de uma função), node list (elementos do Html), ...

   //Utilizando os metodos de um Array em um Array-Like
   function myFunction(){
      //Array.prototype.slice.call...
      //Array.prototype.splice.call...
      //Array.prototype.reduce.call...

      //arguments - Array-like que contém os parametros passados na função
      //utilizando o arguments desta forma, o Array.prototype carrega para dentro o array-like passado por parametro e percorre o mesmo
      Array.prototype.forEach.call(arguments, function(item, index){
         console.log(item);
      });
      myFunction(1,2,3,4,5,6);
   }
   /***********************/

}());
