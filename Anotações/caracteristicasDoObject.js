(function(){
   //Herança - Object.create(); (Aula 12)
   //É utilizado para criar objetos apartir de um obj 'x',
   //onde os novos objetos herdaram as propriedades e métodos do obj 'x'
   //Ex.:
   var obj = {x: 1, y: 2};
   var obj2 = Object.create(obj1); //Neste momento, é criado o obj2, herdando as caracteristicas do obj1

   //Para acessar o valor das propriedades Herdadas
   obj2.x;
   obj2.y;

   //Como foi herdado a propriedade x do obj1, a rotina abaixo irá retornar false
   //Essa rotina serve para verificar se determinada propriedade pertence/nativa do obj
   obj2.hasOwnProperty('x');

   //Métodos do Object
   Object.keys(obj); //Gera um Array com as propriedades do Objeto
   obj.isPrototypeOf(obj2); //Verifica se o obj é protótipo/pai do obj2 -True ou False

   var str = JSON.stringfy(obj); //Converte o obj em uma String no formato JSON **IMPORTANTE**
   var obj = JSON.parse(str);    //Converte a string JSON em um obj **IMPORTANTE**
   //*********************************/

   // Constructor - 15 - 91
   //É possível criar o próprio construtor dos objetos, setando propriedades e métodos.
   //Ex.:
   function Carro(cor, ano){
      this.cor = cor;
      this.ano = ano;
   }
   var fusca = new Carro('Preto', 1978);
   //*********************************/

   // Deletando propriedades - 16 - 96
   delete fusca.cor;


}());
