(function(){
   // SWITCH CASE (Aula 06) *************/
   function myFunc(x){
      switch(x){      //É realizado a comparação exata ===
      case 1:
          console.log('x é igual a 1');
              break;  //Sempre lembrar de implementar o break;
      case 2:
          console.log('x é igual a 2');
              break;
           default:
          console.log('x não é nem 1 nem 2');
      }
   }
   //********************************/

   // Linguagem Funcional (Aula 08) *******************/
   //Como JS é uma linguagem funcional, podemos retornar funções através de outras funções,
   //da mesma forma que é retornado Objetos:
   function adder(x){
      function addOther(y){
         return x + y;
      }
      return addOther;
   }

   //Cria a variável, passando por parametro o valor '2' e recebendo outra função que está dentro da função 'adder()'
   var add2 = adder(2); //add2 = 2

   //Neste momento é passado o parametro para a função addOther() que foi o que foi retornado na chamada da função 'adder()' (chamada anterior)
   add2(3); //add2 = 5

   //Retorna 5, passando os parametros para a função adder() e para a função addOther()
   adder(2)(3);
   //************************************************* */


   //IIFE - Função auto executável (Aula 09) **************/
   //Para poder auto executar uma função literal, deve ser envolvida por parênteses e no final,
   //fazer a chamada para a execução.
   (function() {
      console.log(1 + 2);
   }());
   //******************************************************/


   //typeof - Testando tipos de Valores (Aula 10) *************
   typeof undefined;  //Retorna undefined
   //typeof function(); //Retorna function
   typeof true;       //Retorna boolean
   typeof 10;         //Retorna number
   typeof 'Paulo';    //Retorna string

   typeof {}; //Retorna object
   typeof []; //Retorna object - Utilizar o Array.isArray( array );
   typeof null; //Retorna object **por erro de implementação
   //*********************************************************/

   //Validando o Tipo do Dado - 28 - 167
   //DOM é um objeto criado através de uma função construtora
   DOM.isArray = function isArray(param){
      return Object.prototype.toString.call( param ) === '[object Array]';
   };

   DOM.isObject = function isObject(param){
      return Object.prototype.toString.call( param ) === '[object Object]';
   };

   DOM.isFunction = function isFunction(param){
      return Object.prototype.toString.call( param ) === '[object Function]';
   };

   DOM.isNumber = function isNumber(param){
      return Object.prototype.toString.call( param ) === '[object Number]';
   };

   DOM.isString = function isString(param){
      return Object.prototype.toString.call( param ) === '[object String]';
   };

   DOM.isBoolean = function isBoolean(param){
      return Object.prototype.toString.call( param ) === '[object Boolean]';
   };
   //*********************************************************/

   //(Aula 15- 92) arguments - Método utilizado para retornar um array com todos os parametros que foram passados para a função
   //Ex.:
   function myFunction(arg1, arg2){
      return arguments;
   }
   console.log(myFunction(1, 2)); //[1, 2]
   //*********************************************************/

   //Seção 18 - 106
   //Comparar uma string, retornando algo sem o uso de if e switch
   function getMonthNumber(mes){
      var months = {
         'Janeiro'  : '01',
         'Fevereiro': '02',
         'Março'    : '03',
         'Abril'    : '04',
         'Maio'     : '05',
         'Junho'    : '06',
         'Julho'    : '07',
         'Agosto'   : '08',
         'Setembro' : '09',
         'Outubro'  : '10',
         'Novembro' : '11',
         'Dezembro' : '12',
      }
      return months[ mes ];
   }
   console.log('O mês de Março é representado pelo número '+getMonthNumber('Março')+'.');
   console.log('O mês de Setembro é representado pelo número '+getMonthNumber('Setembro')+'.');
   console.log('O mês de Dezembro é representado pelo número '+getMonthNumber('Dezembro')+'.');
   /*************************************/

   //Prototype - 22 - 134
   //Objetos, Arrays, Strings, Numbers, Functions, ..., herdam de um objeto do tipo 'prototye'
   //O prototype será utilizado para Estender, adicionar novas propriedades e métodos para os objetos.
   //As propriedades declaradas no Objeto se sobrepõe as propriedades estendidas com o ProtoType, pois são lidas primeiro.
   function MyFunction(name, lastName){
      this.name = name;
      this.lastName = lastName;
   }

   MyFunction.prototype.fullName = function(){
      return this.name +'  '+ this.lastName;
   }

   MyFunction.prototype.age = 30;
   MyFunction.prototype.yearOfBirth = 1991;

   var paulo = new MyFunction('Paulo', 'Silvestrin');
   console.log(paulo.fullName()); //Paulo Silvestrin


   //******* 23 - 137 */
   //Passando os valores de um Array como argumentos em uma Função.
   function sum(){
     console.log(arguments);

     //Transforma o 'arguments' em um array para poder percorrer
     return Array.prototype.reduce.call(arguments, (total, item) => {
         return +total+ +item;
     }, 0);
   }

   var numbers = [6,33,5,65,6,50,650];

   //Quebra o Array 'numbers' em argumentos para a própria função 'sum'
   sum.apply(sum, numbers);
   /******************************/

   //**** Manipulando elementos do Doc. HTML - 24 - 143
   //Carrega todos os botões que possuem a tag 'data-js="button-number"'
   var $buttonsNumbers = doc.querySelectorAll('[data-js="button-number"]');

   //Percorre o Array de Botões e injeta a função 'handleClickNumber' em todos os botões numéricos
   //a Função será executada ao clicar sobre os Botões
   Array.prototype.forEach.call($buttonsNumbers, function(button){
      button.addEventListener('click', handleClickNumber, false);
   });

   function handleClickNumber(event){
      //O 'this' é o próprio botão, nesse caso os botões possuem 'value', são de 0 a 9
      $visor.value += this.value;
   }
   /******************************/

   //Técnicas Ninja - 27 - 164
   //Copiando Arrays
   var arr = [1,2,3,4,5];
   var arr2 = arr.slice(); //Utilizando o slice sem os parâmetros, é feito a cópia dos valores do Array

   //Pode ser utilizado em ArrayLikes
   var $divs = document.querySelectorAll('div');
   var $divsCopy = Array.prototype.slice.call($divs);

   //Verificando o Tipo dos Dados
   Object.prototype.toString.call(arr); //[object Array]
   Object.prototype.toString.call(function(){}); //[object Function]
   Object.prototype.toString.call(arguments); //[object Arguments]
   /******************************/

}());
