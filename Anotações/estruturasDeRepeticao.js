(function(){

   //Sintaxe do while ******/
   var iCont = 0;
   while (iCont < 10){
      console.log(iCont);
      iCont++;
   }
   //**********************/

   //Sintaxe do For *********/
   for (var iFor = 0; iFor < 20; iFor++){
      console.log(iFor);
   }

   //Não tem limite de variáveis que possam ser criadas na declaração do for.
   var vlrTotal = 0.00;
   for (var iFor = 0, vlrTotal = 0; iFor < 20; iFor++){
      vlrTotal = 10.00;
      console.log(iFor);
   }
   //**********************/

   //Sintaxe "Do While" *******/
   var counter = 1;
   do {
      console.log(counter++);
   } while (counter < 10);
   //*************************/

   //Sintaxe do "For In" *****************/
   var car = {
      brand: 'VW',
      model: 'Gol',
      year: 2013
   };

   for (var prop in car){
      console.log(prop); //Aqui será impresso o nome das propriedades do objeto
   }
   //******************************/


}());
