(function(){
   //Expressões Regulares 17 - 102
   //- Utilizado para manipular strings;
   //- É um tipo primitido no JS;
   //- Para testar expressões regulares, pode ser utilizado o site: https://regex101.com

   //- Flags
   //g - Faz a Busca em todo o texto
   //i - Ignora Case Sensetive
   //m - Multiline, para fazer o match em strings com várias linhas 19 - 114

   //- Para declarar uma expressão regular em JS, basta informar a expressão entre as barras //;
   var regex = /m/;
   var texto = 'Esse texto tem a finalidade apenas de demonstração 1875';
   texto.match(/m/); //Busca o primeiro 'm' minusculo do texto - Retorna um array
   texto.match(/m/g); //Busca o TODOS os 'm' minusculo do texto - Retorna um array
   texto.match(/m/gi); //Busca o TODOS os 'm' ignorando o case sensetive - Retorna um array
   texto.match(/----/gi); //Se não encontrar, é retornado null

   //- Termos
   //  \w - Caracteres alfanuméricos e _
   //  \d - Números (digitos)
   //  \s - Espaços em Branco
   //  \n - Quebra de Linha
   //   . - Todos os caracteres que não sejam quebra de linha
   //  \W - Faz o match com todos os caracteres que não forem alfanuméricos e digitos (Caracter de NEGAÇÃO)
   //  \D - Faz o match com todos os caracteres que não forem digitos (Caracter de NEGAÇÃO)
   //  \S - Faz o match com todos os caracteres que, MENOS os espaços em branco (Caracter de NEGAÇÃO)

   texto.match(/\w/g); //Retorna um Array com todas as letras que compõe o Texto
   texto.match(/\d/g); //Retorna um Array com todos os numeros que compõe o Texto
   texto.match(/\d\d/g); //["18", "75"] - Retorna um Array casando dois números

   //- Classes de Caracteres 17 - 103
   texto.match(/a|b/g); //Retorna um Array com todas as letras 'a' e 'b' que compõe o Texto
   texto.match(/[abcd]/g); //Retorna um Array com todas as letras 'a', 'b', 'c' e 'd' que compõe o Texto, assim como o exemplo acima
   texto.match(/[123]/g);

   //- Agrupamento de Caracteres
   texto.match(/123|456/g); //Retorna um Array com todos os grupos de letras '123' e '456' que compõe o Texto

   //- Sequência de Caracteres
   texto.match(/[0-9]/g); //Pega todos os numeros de 0 a 9
   texto.match(/[A-Za-z0-9]/g); //Pega todas as letras de A a Z e todos os numeros de 0 a 9
   //***************************/

   //- Utilizando Regex com Replace; //17 - 104
   texto.replace(/de/g, 'DE'); //Altera em todo o texto tudo o que for 'de' para 'DE'
   texto.replace(/(d)(e)/g, '--$1'); //Altera em todo o texto tudo o que for 'de' para '--d', pois o $1 armazena o valor do primeiro caracter informado
   texto.replace(/(d)(e)/g, '$1$2'); //Altera em todo o texto tudo o que for 'de' para 'de'
   texto.replace(/(d)(e)/g, function(captura, d, e){  //Utilizando o regex com o Replace, pode ser criado uma função
      return (d + e).toUpperCase();
   });

   //Retorna Paulo todo em Maiusculo, passando uma letra por vez
   'Paulo'.replace(/(\w)/g, function(capturaTotal, letra){
      return letra.toUpperCase();
   });

   //Retorna Paulo intercalando entre letras maiusculas e minusculas
   'Paulo'.replace(/(\w)(\w)/g, function(capturaTotal, letra, letra2){
      return letra.toUpperCase() + letra2.toLowerCase();
   });
   //**************************/

   //Caracteres de Negação ************** (18 - 109, 110)/
   //O caracter deve estar sempre dentro da Lsta, fora da lista, funciona como um caracter de inicio de String 19 - 118
   texto.match(/[^abc]/g); //Pega qualquer caracter que NÃO seja a,b ou c

   //Repetidores ***********//
   //Repetidor de Intervalos
   texto.match(/\d{2,4}/g); //Faz match com números com no minimo 2 e no máximo 4 digitos

   //Repetidor de Intervalo Aberto
   texto.match(/\d{3,}/g); //Faz match com TODOS os números que possuem no minimo 3 digitos

   //Repetidor Exato
   texto.match(/\d{4}/g); //Faz match com TODOS os números que possuem EXATAMENTE 4 digitos
   //**********************/

   //Caracter de Opcional
   texto.match(/\d\d\d?/g); //Faz match com TODOS os números que possuem ENTRE 2 e 3 digitos
   texto.match(/\s\d?/g);   //Faz match com espaço em branco e um numero, porém, o número é opcional

   //Uma ou mais ocorrências do Item Anterior
   texto.match(/s+/g); //Faz match com UM ou MAIS 's' (Ace'ss'ório, Pa'ss'ado, A'ss'u's'tador...)
   texto.match(/\w\w+/g); //Faz match onde ocorrer no minimo DUAS letras ou MAIS
   texto.match(/\w\w\w+/g); //Faz match onde ocorrer no minimo TRÊS letras ou MAIS

   //Qualquer caracter
   texto.match(/w*/g); //Faz match com QUALQUER caracter

   // *** Challenge 18 ******************
   /* Usando os CPFs limpos acima, deixe-os com a formatação correta de CPF.
   Ex.: "999.999.999-99"
   Mostre o resultado no console. */
   console.log('\nFormatando CPFs corretamente:');
   cpfs.forEach(function( cpf ){
      console.log(cleanCPF( cpf ).replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1.$2.$3-$4'));
      //OU
      /*console.log(cleanCPF( cpf ).replace(/(\d{3})(\d{3})(\d{3})(\d{2})/,
            function(regex, arg1, arg2, arg3, arg4){ //arg1, 2, 3 e 4 são as capturas de texto (estão entre os parenteses)
               return arg1 +'.'+ arg2 +'.'+ arg3 +'-'+ arg4;
            })); */
      //OU
      //console.log(cleanCPF( cpf ).replace(/(\d\d\d)(\d\d\d)(\d\d\d)(\d\d)/, '$1.$2.$3-$4'));
   });

   /*Vamos complicar um pouco agora :D
   Crie uma expressão regular que faça o match com um texto existente dentro de
   uma tag HTML. O texto deve ser capturado e substituído por:
   'O texto dentro da tag "[NOME DA TAG]" é "[TEXTO]"'

   Use a marcação abaixo para fazer o replace:
   "<h1>Título da página</h1><p>Este é um parágrafo</p><footer>Rodapé</footer>"

   O resultado deve ser esse:
   <h1>O texto dentro da tag "h1" é "Título da página"</h1>
   <p>O texto dentro da tag "p" é "Este é um parágrafo"</p>
   <footer>O texto dentro da tag "footer" é "Rodapé"</footer>*/
   console.log( '\nFazer replace dos textos das tags:' );
   var codHtml = '<h1>Título da página</h1><p>Este é um parágrafo</p><footer>Rodapé</footer>';

   console.log(codHtml.replace(/<(\w+)>([^<]+)<\/\w+>/g,
                  '<$1>O texto dentro da tag "$1" é "$2"</$1>\n'
            ));
   //******************************************************************/

   //Conhecendo Outros Símbolos usados na Regex 19 - 114
   var texto = '<h1>Título da página</h1><p>Este é um parágrafo</p><footer>Rodapé</footer>';
   texto.match(/^</);  //Verifica se a String INICIA com o valor '<' (sempre da primeira linha)
   texto.match(/^</m); //Verifica se a String INICIA com o valor '<' (se tiver quebra de linha, verifica se todas as linhas iniciam com '<')

   texto.match(/>$/); //Verifica se a String TERMINA com o valor '>'

}());
