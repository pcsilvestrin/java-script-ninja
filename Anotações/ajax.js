(function(){
   'use strict'

   //AJAX - Asynchronous Javascript and XML /* 28 - 169 */
   //Utilizado para realizar requisições via http, para api's do mesmo ou de diferente endereço, que retornará algum arquivo, xml, Json, txt, html...
   //Será utilizado o componente:
   window.XMLHttpRequest();

   var ajax = new XMLHttpRequest();

   //Abre a conexão com a API, passando o protocolo Http (get, post, put, delete)
   //ajax.open(<protocol>, <url>);

   //Faz o envio da requisição
   //ajax.send(<data>);

   /* 28 - 170 */
   //Evento que fica 'ouvindo' se o status da requisição foi alterado, no caso, se terminou a requisição
   ajax.addEventListener('readystatechange', function(){
      console.log('Terminou a requisição! ', ajax.readyState, ajax.status);
   }, false);

   //Retorna o Status da Requisição
   //0 : Não Enviado
   //1 : Conexão Aberta
   //2 : Headers Carregados e Recebidos
   //3 : Carregando o corpo do Request
   //4 : Requisição concluída com Sucesso
   ajax.readyState;

   //Retorna o status http referente a requisição
   //200, 403, 404, 500, ...
   ajax.status;

   /* 28 - 171 */
   //Manipulando a resposta das Requisições Ajax
   //É utilizado os eventos abaixo para capturar o retorno da requisição;
   //ajax.responseText - Quando o retorno é um arquivo de texto, html, JSON
   //ajax.responseXML - Quando o retorno é um arquivo XML (nesse caso não é necessário converter para JSON)

   console.log('Carregando ...');
   ajax.addEventListener('readystatechange', function(){
      //Converte o retorno obtido na requisição em JSON, já que o retorno veio tipo string
      var data = JSON.parse(ajax.responseText);
      if ( isRequestOk() ){
         console.log('Requisição Ok!', data.message);
      }
   }, false);

   function isRequestOk(){
      //Verifica se a Requisição foi concluida com sucesso e se o status retornado foi o 200
      return ajax.readyState === 4 && ajax.status === 200;
   }

   /* 28 - 172 */
   //Tratamento de Erros
   try {
     //Bloco de Código em que será tratado algum erro que possa ocorrer
   } catch(e) {
     //e - Captura o erro para que possa ser tratado, neste caso, só foi impresso no console
     console.log(e);
   }

   //Disparando erros no JS
   throw new Error('Mensagem de erro!');
   throw new SintaxeError('Erro de Sintaxe!');

}());
