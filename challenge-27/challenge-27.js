(function(doc, win){
   /*
   Aproveitando a lib DOM que fizemos na semana anterior, crie agora para ela
   métodos semelhantes aos que existem no array, mas que sirvam para os
   elementos do DOM selecionados.
   Crie os seguintes métodos:
   - forEach, map, filter, reduce, reduceRight, every e some.

   Crie também métodos que verificam o tipo do objeto passado por parâmetro.
   Esses métodos não precisam depender de criar um novo elmento do DOM, podem
   ser métodos estáticos.

   Métodos estáticos não obrigam o uso do `new`, podendo ser usados diretamente
   no objeto, como nos exemplos abaixo:
   DOM.isArray([1, 2, 3]); // true
   DOM.isFunction(function() {}); // true
   DOM.isNumber('numero'); // false

   Crie os seguintes métodos para verificação de tipo:
   - isArray, isObject, isFunction, isNumber, isString, isBoolean, isNull.
   O método isNull deve retornar `true` se o valor for null ou undefined.
   */
   function DOM(elements){
      //Carrega um array-like com todos os elementos encontrados apartir do parametro passado
      this.element = document.querySelectorAll(elements);
   }

   //Adiciona o evento 'on' em todos os elementos, será executado ao clicar sobre o elemento
   DOM.prototype.on = function on(eventType, callback){
      //Percorre uma lista de elementos e injeta um evento callback, para cada elemento
      Array.prototype.forEach.call(this.element, function(item){
         item.addEventListener(eventType, callback, false);
      });
   };

   //Remove o evento 'on' em todos os elementos, será executado ao clicar sobre o elemento
   DOM.prototype.off = function off(eventType, callback){
      //Percorre uma lista de elementos e REMOVE o evento de CLICK
      Array.prototype.forEach.call(this.element, function(item){
         item.removeEventListener(eventType, callback, false);
      });
   };

   DOM.prototype.get = function get(){
      return this.element;
   };

   DOM.prototype.forEach = function forEach(){
      //this.element - É o arrayLike criado ao instanciar
      //arguments - É a função passada por parametro, ao chamar esta função
      return Array.prototype.forEach.apply(this.element, arguments);
   };

   DOM.prototype.map = function map(){
      //this.element - É o arrayLike criado ao instanciar
      //arguments - É a função passada por parametro, ao chamar esta função
      return Array.prototype.map.apply(this.element, arguments);
   };

   DOM.prototype.filter = function reduce(){
      //this.element - É o arrayLike criado ao instanciar
      //arguments - É a função passada por parametro, ao chamar esta função
      return Array.prototype.filter.apply(this.element, arguments);
   };

   DOM.prototype.reduce = function reduce(){
      //this.element - É o arrayLike criado ao instanciar
      //arguments - É a função passada por parametro, ao chamar esta função
      return Array.prototype.reduce.apply(this.element, arguments);
   };

   DOM.prototype.reduceRight = function reduceRight(){
      //this.element - É o arrayLike criado ao instanciar
      //arguments - É a função passada por parametro, ao chamar esta função
      return Array.prototype.reduceRight.apply(this.element, arguments);
   };

   DOM.prototype.every = function every(){
      //this.element - É o arrayLike criado ao instanciar
      //arguments - É a função passada por parametro, ao chamar esta função
      return Array.prototype.every.apply(this.element, arguments);
   };

   DOM.prototype.some = function some(){
      //this.element - É o arrayLike criado ao instanciar
      //arguments - É a função passada por parametro, ao chamar esta função
      return Array.prototype.some.apply(this.element, arguments);
   }

   //Validando o Tipo do Dado - 28 - 167
   //DOM é um objeto criado através de uma função construtora
   DOM.isArray = function isArray(param){
      return Object.prototype.toString.call( param ) === '[object Array]';
   };

   DOM.isObject = function isObject(param){
      return Object.prototype.toString.call( param ) === '[object Object]';
   };

   DOM.isFunction = function isFunction(param){
      return Object.prototype.toString.call( param ) === '[object Function]';
   };

   DOM.isNumber = function isNumber(param){
      return Object.prototype.toString.call( param ) === '[object Number]';
   };

   DOM.isString = function isString(param){
      return Object.prototype.toString.call( param ) === '[object String]';
   };

   DOM.isBoolean = function isBoolean(param){
      return Object.prototype.toString.call( param ) === '[object Boolean]';
   };

   DOM.isNull = function isNull(param){
      return Object.prototype.toString.call( param ) === '[object Null]'
             || Object.prototype.toString.call( param ) === '[object Undefined]';
   };

   var $a = new DOM('[data-js="link"]');

   $a.forEach(function(item){
      console.log(item.firstChild.nodeValue);
   });


   var dataJs = $a.map(function(item){
      return item.getAttribute('data-js');
   });
   console.log(dataJs);

}(document, window));
