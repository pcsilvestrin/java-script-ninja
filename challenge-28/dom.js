(function(win){
   'use strict';

   function DOM(elements){
      //Carrega um array-like com todos os elementos encontrados apartir do parametro passado
      this.element = document.querySelectorAll(elements);
   }

   //Adiciona o evento 'on' em todos os elementos, será executado ao clicar sobre o elemento
   DOM.prototype.on = function on(eventType, callback){
      //Percorre uma lista de elementos e injeta um evento callback, para cada elemento
      Array.prototype.forEach.call(this.element, function(item){
         item.addEventListener(eventType, callback, false);
      });
   };

   //Remove o evento 'on' em todos os elementos, será executado ao clicar sobre o elemento
   DOM.prototype.off = function off(eventType, callback){
      //Percorre uma lista de elementos e REMOVE o evento de CLICK
      Array.prototype.forEach.call(this.element, function(item){
         item.removeEventListener(eventType, callback, false);
      });
   };

   DOM.prototype.get = function get(){
      return this.element;
   };

   DOM.prototype.forEach = function forEach(){
      //this.element - É o arrayLike criado ao instanciar
      //arguments - É a função passada por parametro, ao chamar esta função
      return Array.prototype.forEach.apply(this.element, arguments);
   };

   DOM.prototype.map = function map(){
      //this.element - É o arrayLike criado ao instanciar
      //arguments - É a função passada por parametro, ao chamar esta função
      return Array.prototype.map.apply(this.element, arguments);
   };

   DOM.prototype.filter = function reduce(){
      //this.element - É o arrayLike criado ao instanciar
      //arguments - É a função passada por parametro, ao chamar esta função
      return Array.prototype.filter.apply(this.element, arguments);
   };

   DOM.prototype.reduce = function reduce(){
      //this.element - É o arrayLike criado ao instanciar
      //arguments - É a função passada por parametro, ao chamar esta função
      return Array.prototype.reduce.apply(this.element, arguments);
   };

   DOM.prototype.reduceRight = function reduceRight(){
      //this.element - É o arrayLike criado ao instanciar
      //arguments - É a função passada por parametro, ao chamar esta função
      return Array.prototype.reduceRight.apply(this.element, arguments);
   };

   DOM.prototype.every = function every(){
      //this.element - É o arrayLike criado ao instanciar
      //arguments - É a função passada por parametro, ao chamar esta função
      return Array.prototype.every.apply(this.element, arguments);
   };

   DOM.prototype.some = function some(){
      //this.element - É o arrayLike criado ao instanciar
      //arguments - É a função passada por parametro, ao chamar esta função
      return Array.prototype.some.apply(this.element, arguments);
   }

   //Validando o Tipo do Dado - 28 - 167
   //DOM é um objeto criado através de uma função construtora
   DOM.isArray = function isArray(param){
      return Object.prototype.toString.call( param ) === '[object Array]';
   };

   DOM.isObject = function isObject(param){
      return Object.prototype.toString.call( param ) === '[object Object]';
   };

   DOM.isFunction = function isFunction(param){
      return Object.prototype.toString.call( param ) === '[object Function]';
   };

   DOM.isNumber = function isNumber(param){
      return Object.prototype.toString.call( param ) === '[object Number]';
   };

   DOM.isString = function isString(param){
      return Object.prototype.toString.call( param ) === '[object String]';
   };

   DOM.isBoolean = function isBoolean(param){
      return Object.prototype.toString.call( param ) === '[object Boolean]';
   };

   DOM.isNull = function isNull(param){
      return Object.prototype.toString.call( param ) === '[object Null]'
            || Object.prototype.toString.call( param ) === '[object Undefined]';
   };

   //Adicionar a declaração do DOM em escopo Global
   //No arquivo JS que utiliza o DOM, deve ser feito a importação,
   //declarando o 'window.DOM'
   win.DOM = DOM;
}(window));
