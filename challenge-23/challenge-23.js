(function(doc, win){
   'use strict'
   /*
   Vamos desenvolver mais um projeto. A ideia é fazer uma mini-calculadora.
   As regras são:

   - Deve ter somente 1 input, mas não deve ser possível entrar dados nesse input
   diretamente;
   - O input deve iniciar com valor zero;
   - Deve haver 10 botões para os números de 0 a 9. Cada botão deve ser um número;
   - Deve haver 4 botões para as operações principais: soma (+), subtração(-),
   multiplicação(x) e divisão(÷);
   - Deve haver um botão de "igual" (=) que irá calcular os valores e um botão "CE"
   que irá limpar o input, deixando-o com valor 0;

   - A cada número pressionado, o input deve atualizar concatenando cada valor
   digitado, como em uma calculadora real;
   - Ao pressionar um botão com uma das 4 operações, deve aparecer o símbolo da
   operação no input. Se o último caractere no input já for um símbolo de alguma
   operação, esse caractere deve ser substituído pelo último pressionado.
   Exemplo:
   - Se o input tem os valores: "1+2+", e for pressionado o botão de
   multiplicação (x), então no input deve aparecer "1+2x".
   - Ao pressionar o botão de igual, o resultado do cálculo deve ser mostrado no
   input;
   - Ao pressionar o botão "CE", o input deve ficar zerado.
   */
   var $visor = doc.querySelector('[data-js=visor]');
   var $buttonCE = doc.querySelector('[data-js="button-ce"]');
   var $buttonEqual = doc.querySelector('[data-js="button-equal"]');

   //Carrega todos os botões com a tag 'data-js="button-number"'
   var $buttonsNumbers = doc.querySelectorAll('[data-js="button-number"]');
   var $buttonsOperations = doc.querySelectorAll('[data-js="button-operation"]');

   //Percorre o Array de Botões e injeta a função 'handleClickNumber' em todos os botões numéricos
   //a Função será executada ao clicar sobre os Botões
   Array.prototype.forEach.call($buttonsNumbers, function(button){
      button.addEventListener('click', handleClickNumber, false);
   });
   Array.prototype.forEach.call($buttonsOperations, function(button){
      button.addEventListener('click', handleClickOperation, false);
   });

   $buttonCE.addEventListener('click', handleClickCE, false);
   $buttonEqual.addEventListener('click', handleClickEqual, false);

   function handleClickNumber(){
      $visor.value += this.value;
   }

   function handleClickOperation(){
      $visor.value = removeLastItemIfItIsOperator($visor.value);
      $visor.value += this.value;
   }

   function handleClickCE(){
      $visor.value = '0'
   }

   function isLastItemAnOperation(number){
      //Converte o valor do 'visor' em um Array e pega o ultimo item
      var lastItem = number.split('').pop();
      var operacoes = ['+', '-', 'x', '/'];

      return operacoes.some( (value) => {
         return value === lastItem;
      });
   }

   function handleClickEqual(){ //147
      $visor.value = removeLastItemIfItIsOperator($visor.value);

      var allValues = $visor.value.match(/\d+[+x/-]?/g);
      var result = allValues.reduce(function(accumulated, actual){
         var firstValue = accumulated.slice(0, -1);
         var operator = accumulated.split('').pop();
         var lastValue = removeLastItemIfItIsOperator(actual);
         var lastOperator = isLastItemAnOperation(actual) ? actual.split('').pop() : '';

         switch (operator){
            case 'x':
               return (Number(firstValue) * Number(lastValue)) + lastOperator;
            case '+':
               return (Number(firstValue) + Number(lastValue)) + lastOperator;
            case '-':
               return (Number(firstValue) - Number(lastValue)) + lastOperator;
            case '/':
               return (Number(firstValue) / Number(lastValue)) + lastOperator;
         }
      });

      $visor.value = result;
   }

   function removeLastItemIfItIsOperator(number){
      //Se o ultimo caracter for de uma operação, ele é removido
      if (isLastItemAnOperation(number)){
         return number.slice(0, -1);
      }
      return number;
   }

}(document, window));
