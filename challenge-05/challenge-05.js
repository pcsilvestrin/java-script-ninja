/*
Crie uma variável qualquer, que receba um array com alguns valores aleatórios
- ao menos 5 - (fica por sua conta os valores do array).
*/
var valores = [3, NaN, 'Teste', null, 1];

/*
Crie uma função que receba um array como parâmetro, e retorne esse array.
*/
function getArray(arg){
  return arg;
}

/*
Imprima o segundo índice do array retornado pela função criada acima.
*/
console.log(getArray(valores)[1]);

/*
Crie uma função que receba dois parâmetros: o primeiro, um array de valores; e o
segundo, um número. A função deve retornar o valor de um índice do array que foi passado
no primeiro parâmetro. O índice usado para retornar o valor, deve ser o número passado no
segundo parâmetro.
*/
function getIndice(arr, iIndx){
  return arr[iIndx];
}

/*
Declare uma variável que recebe um array com 5 valores, de tipos diferentes.
*/
var valores2 = [3, NaN, null, 'a', 6];

/*
Invoque a função criada acima, fazendo-a retornar todos os valores do último
array criado.
*/
console.log(getIndice(valores2, 0));
console.log(getIndice(valores2, 1));
console.log(getIndice(valores2, 2));
console.log(getIndice(valores2, 3));
console.log(getIndice(valores2, 4));

/*
Crie uma função chamada `book`, que recebe um parâmetro, que será o nome do
livro. Dentro dessa função, declare uma variável que recebe um objeto com as
seguintes características:
- esse objeto irá receber 3 propriedades, que serão nomes de livros;
- cada uma dessas propriedades será um novo objeto, que terá outras 3
propriedades:
    - `quantidadePaginas` - Number (quantidade de páginas)
    - `autor` - String
    - `editora` - String
- A função deve retornar o objeto referente ao livro passado por parâmetro.
- Se o parâmetro não for passado, a função deve retornar o objeto com todos
os livros.
*/
function book(livro){
  var obj = {
    'Java' : {quantidadePaginas: 700, autor: 'Ele', editora: 'Record'},
    'Mais Esperto que o Diabo' : {quantidadePaginas: 305, autor: 'Napolleon Hill', editora: 'Globo'},
    'Sangue' : {quantidadePaginas: 80, autor: 'Desconhecido', editora: 'Aurora'}
  };

  //Se foi passado o nome do livro por parametro
  //é pego o livro pelo nome
  return !livro ? obj : obj[livro];
}

/*
Usando a função criada acima, imprima o objeto com todos os livros.
*/
console.log(book('teste'));

/*
Ainda com a função acima, imprima a quantidade de páginas de um livro qualquer,
usando a frase:
"O livro [NOME_DO_LIVRO] tem [X] páginas!"
*/
console.log('O livro '+book('Sangue').nome+' possui '+book('Sangue').quantidadePaginas+' páginas!');

/*
Ainda com a função acima, imprima o nome do autor de um livro qualquer, usando
a frase:
"O autor do livro [NOME_DO_LIVRO] é [AUTOR]."
*/
console.log('O autor do livro '+book('Mais Esperto que o Diabo').nome+' é '+book('Mais Esperto que o Diabo').autor+'!');

/*
Ainda com a função acima, imprima o nome da editora de um livro qualquer, usando
a frase:
"O livro [NOME_DO_LIVRO] foi publicado pela editora [NOME_DA_EDITORA]."
*/
console.log('O livro '+book('Mais Esperto que o Diabo').nome+' foi publicado pela editora '+book('Mais Esperto que o Diabo').editora+'!');
